# -*- coding: utf-8 -*-
import re
import urllib2
from urllib import urlencode
from datetime import datetime
from cookielib import CookieJar
from BeautifulSoup import BeautifulSoup

from django.utils.encoding import smart_unicode, force_unicode

profiles = (
    ('myaccount', 'mypasswd'),
)

messagelist_re = re.compile("(?:(START of MESSAGE LIST -->))(.*?)(?:(<!-- END of MESSAGE LIST))")

def read_inbox(username):
    print "Reading messages.",
    f = open('/tmp/datebox/message_list_%s.html' % username, 'r')
    unicode_source = unicode(f.read(), 'ISO-8859-1')
    soup = BeautifulSoup(unicode_source)
    unreads = soup('a', {'class': 'unread'})
    if len(unreads) > 0:
        print "New messages!"
        for message in unreads:
            print " * %s" % message.string.strip().encode('utf8')
        print " "
    else:
        print "No new messages."

if __name__ == "__main__":
    now = datetime.now()
    if now.minute < 10:
        minutes = '0%s' % now.minute
    else:
        minutes = now.minute
    print "Running %s.%s. at %s.%s" % (now.day, now.month, now.hour, minutes)
    for username, password in profiles:
        cj = CookieJar()
        opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
        login_data = urlencode({
            'name': username, 
            'pass': password,
            'form_id': 'user_login',
        })

        print "Logging in %s." % username,
        opener.open('https://oma.suomi24.fi/user/login?wanted_url=http://treffit.suomi24.fi/', login_data)
        print "Done.",
        resp = opener.open('http://treffit.suomi24.fi/message.do?method=listReceived')
    
        f = open('/tmp/datebox/message_list_%s.html' % username, 'w')
        f.write(resp.read())
        f.close()
        read_inbox(username=username)
